# Defqon.1 2024 Live Broadcast

Data files regarding live broadcast, that took place on June 27-30 (jump to [Day 1](#day-1---thursday-june-27), [Day 2](#day-2---friday-june-28), [Day 3](#day-3---saturday-june-29), [Day 4](#day-4---sunday-june-30))

The project contains dirs with sfv, nfo & jpg files inside

## DAY 1 - Thursday, June 27

**BLUE**

- 00:00:00  Intro
- 00:21:50  Sound Rush
- 01:33:00  Hard Driver
- 02:33:00  B-Front x Phuture Noize: The Enlightenment
- 03:18:20  Act Of Rage
- 04:31:30  Brennan Heart

**BLACK**

- 00:00:00  Mad Dog
- 01:06:20  Fantastic Four: Panic, The Darkraver, The Viper, Vince
- 02:03:34  Miss K8
- 02:59:25  Deadly Guns
- 04:14:55  Paul Elstak & Partyraiser


**INDIGO**

- 00:00:00  RBX
- 00:51:30  Griever & PL4Y
- 01:41:08  Sanctuary
- 02:16:00  Coldax
- 03:01:48  The Purge
- 03:46:19  Omnya
- 04:29:30  Gezellige Uptempo

**MAGENTA**

- 00:20:25  MickeyG & TWSTD
- 01:23:00  Revelators
- 02:18:37  Solstice 'Intense Euphower'
- 03:17:47  MVTATE
- 04:13:34  Ecstatic
- 05:16:15  Jones & Jay Reeve - Back To The Future


## Video content

This year, there will be a free video stream on the Q-dance YouTube channel on the Red stage for Friday, Saturday, and Sunday.

## Audio content

All audio content of Day 1 can be downloaded from [1gabba.pw](https://1gabba.pw/node/78474):
- VA_-_Defqon.1_2024_Day_1_Black-WEB-2024
- VA_-_Defqon.1_2024_Day_1_Blue-WEB-2024
- VA_-_Defqon.1_2024_Day_1_Indigo-WEB-2024
- VA_-_Defqon.1_2024_Day_1_Magenta-WEB-2024

***

## DAY 2 - Friday, June 28

**RED**

- 00:30:00 The Opening Ceremony with Ran-D
- 01:20:15 DJ Isaac
- 02:19:20 Tatanka
- 03:20:05 Da Tweekaz
- 04:19:50 D-Sturb
- 05:04:10 Showtek
- 05:49:45 Vertile
- 06:35:30 Sub Zero Project
- 07:20:20 Rebelion
- 08:04:55 Dreamscape by The Purge & Adjuzt
- 09:39:00 The Spotlight With This Is Sefa

**BLUE**

- 00:00:00 Omnya & Sanctuary
- 01:03:28 Resolute
- 02:02:57 RVAGE
- 03:04:40 Digital Punk presents Cyber Punk
- 03:30:40 Kronos presents Kryptonite 2.0
- 04:16:03 Voidax
- 05:18:20 E-Force LIVE
- 05:46:00 Aversion
- 06:45:40 Gunz For Hire
- 07:31:25 Cryex presents Band of Color
- 08:17:00 Killshot
- 09:04:00 Unresolved: Bad Blood LIVE
- 09:44:50 Sickmode
- 10:16:00 Malice presents: My Therapy

**BLACK**

- 00:00:00 Unfused
- 00:55:30 Hardcore Confessions with Audiofreq
- 01:41:00 Holy Shit
- 01:53:50 Furyan
- 02:39:40 Re-Style
- 03:24:15 Restrained presents Rulebreaking Hardcore LIVE
- 03:54:18 Hysta
- 04:53:55 Nosferatu
- 05:53:50 Bulletproof presents Fearless
- 06:53:57 Dither
- 07:24:55 Billx
- 08:10:00 Cryogenic presents The Next Gen LIVE
- 08:56:55 Lil Texas
- 09:55:55 Barbaric Records Live: Barber, Unproven, Manifest Destiny
- 11:40:05 The Spotlight with Spitnoise

**GOLD**

- 00:27:40 Pavo
- 02:01:03 T-Go
- 03:00:00 Painbringer
- 04:00:15 Charly Lownoise
- 05:30:35 Dune
- 06:28:48 Tommyknocker
- 07:30:11 Endymion QORE 3.0 set
- 08:30:25 Ruffneck & Ophidian
- 09:30:05 Vandal!sm

**GREEN**

- 00:00:00 Ricardo Carota
- 00:28:31 DAE
- 01:29:05 Ghost In The Machine
- 02:29:05 Somniac One
- 03:56:55 Cancel
- 05:27:10 Luciid
- 06:58:45 APHØTIC
- 08:27:55 Cynthia Spiering
- 10:29:02 Karah

**INDIGO**

- 00:19:20 Revolve
- 01:16:00 K1
- 02:16:21 Bright Visions
- 02:46:16 Dark Entities & Required
- 03:31:05 Alter Ego
- 04:16:10 Griever
- 05:15:44 Imperatorz
- 06:16:00 Oxya
- 07:15:51 Vexxed
- 08:15:55 Vasto & Luminite
- 09:15:21 Coldax
- 09:58:54 Kenai LIVE
- 10:15:05 Unfold Showcase: Sparkz, BMBERJCK, Required, Heavy Resistance
- 11:18:00 Kruelty

**MAGENTA**

- 00:00:00 Jajox
- 00:30:34 Jason Payne - Goldschool
- 01:30:08 Degos & Re-Done
- 02:31:50 Chain Reaction - Minus is More Special
- 03:29:45 Sub Sonik
- 04:29:21 Adaro
- 05:30:00 Deetox
- 06:29:00 Wild Motherfuckers
- 07:13:00 E-Force & Crypsis
- 08:21:00 Mind Dimension
- 09:12:32 High Voltage
- 09:47:48 Unresolved & YRS

**ORANGE**

- 00:09:26 Liquid Acid
- 01:38:00 Naadt
- 03:26:50 stream technical issues
- 03:39:13 Tulpa Dusha
- 05:07:20 Back To Mars
- 06:38:00 Jorine
- 08:50:20 Errol Du Ville

**UV**

- 00:00:00 Firelite
- 00:31:37 Jay Reeve presents Melodic Madness
- 01:36:34 Avi8
- 02:16:05 Wasted Penguinz presents WSTD
- 02:45:06 Sephyx & Stormerz
- 03:40:35 stream technical issues
- 04:16:27 Mandy
- 04:30:05 Andy Svge presents Ravetrip
- 05:11:20 Max Enforcer
- 05:29:45 Keltek Live
- 06:16:00 Atmozfears presents Abstrakt
- 06:59:00 Brennan Heart presents Save Euphoric
- 07:31:00 3 Blokes (Code Black, Toneshifterz, Audiofreq)
- 08:15:05 The Viper - Ultimate Vibe
- 08:46:00 Frequencerz presents Deja Vu
- 09:30:15 Tweekacore
- 10:23:00 Tripping’ with Dr. Peacock

**YELLOW**

- 00:00:00 Vertex
- 00:29:47 Mind Compressor
- 01:17:15 Crypton
- 02:13:30 Unlocked
- 03:12:14 Spiady
- 03:58:27 Lunakorpz
- 04:57:22 System Overload
- 05:28:05 Sprinky & Deathroar
- 06:13:30 Trespassed
- 06:58:55 F.Noize
- 07:44:00 Soulblast Hard Curse Live
- 08:19:03 Yoshiko
- 09:10:22 Chaotic Hostility
- 09:42:26 The Vizitor


## Video content

Video content of Day 2 RED stage can be downloaded from [1gabba.pw](https://1gabba.pw/node/78576):

- Defqon.1.2024.Day.2.Red.1080p.WEB.x264

- D-Sturb.RED.Friday.Defqon1.2024.2160p.mp4
- Da-Tweekaz.RED.Friday.Defqon1.2024.2160p.mp4
- DJ-ISAAC.RED.Friday.Defqon1.2024.2160p.mp4
- Dreamscape-The-Purge-Adjuzt.RED.Friday.Defqon1.2024.2160p.mp4
- Ran-D.RED.Friday.Defqon1.2024.2160p.mp4
- Rebelion.RED.Friday.Defqon1.2024.2160p.mp4
- Rejecta.RED.Friday.Defqon1.2024.2160p.mp4
- Sefa.RED.Friday.Defqon1.2024.2160p.mp4
- Showtek.RED.Friday.Defqon1.2024.2160p.mp4
- Sub-Zero-Project.RED.Friday.Defqon1.2024.2160p.mp4
- Tatanka.RED.Friday.Defqon1.2024.2160p.mp4
- Vertile.RED.Friday.Defqon1.2024.2160p.mp4


## Audio content

All audio content of Day 2 can be downloaded from [1gabba.pw](https://1gabba.pw/node/78576):

- VA_-_Defqon.1_2024_Day_2_Black-WEB-2024
- VA_-_Defqon.1_2024_Day_2_Blue-WEB-2024
- VA_-_Defqon.1_2024_Day_2_Gold-WEB-2024
- VA_-_Defqon.1_2024_Day_2_Green-WEB-2024
- VA_-_Defqon.1_2024_Day_2_Indigo-WEB-2024
- VA_-_Defqon.1_2024_Day_2_Magenta-WEB-2024
- VA_-_Defqon.1_2024_Day_2_Orange-WEB-2024
- VA_-_Defqon.1_2024_Day_2_Red-WEB-2024
- VA_-_Defqon.1_2024_Day_2_UV-WEB-2024
- VA_-_Defqon.1_2024_Day_2_Yellow-WEB-2024



## DAY 3 - Saturday, June 29

**BLACK**

- 00:27:34 Chaos Project
- 01:42:10 Marc Acardipane & Rob Gee - Are The Gabberanos
- 02:27:11 Broken Minds
- 03:45:40 AniMe
- 04:27:01 Lunakorpz
- 05:27:14 Karun
- 06:26:55 N-Vitral
- 07:56:35 Black Reaper presents: Deadly Guns x Elite Enemy x Heavy Damage
- 08:57:03 Major Conspiracy & The Dope Doctor
- 09:47:31 Irradiate presents Atomic Warfare
- 10:27:38 MBK presents World of Distortion
- 11:05:52 DRS Live
- 11:33:00 Dimitri K

**BLUE**

- 00:37:45 Level One (low volume till 01:31:00)
- 02:01:20 Scarra & Vasto present Bassline Breaker
- 03:30:05 Code Black
- 03:49:20 Deluzion
- 04:30:45 Aftershock
- 05:30:20 Chapter V
- 06:30:20 Dual Damage
- 07:15:55 The Straikerz - Jackpot.1 Live
- 07:46:13 Anderex
- 08:30:25 The Purge presents Beyond Trippin'
- 09:00:45 Mutilator
- 10:01:00 Adjuzt
- 10:44:06 Riot Shift

**GOLD**

- 00:18:25 Artcore with Ruffneck
- 01:20:27 Noxa
- 02:19:00 DJ Rob & MC Joe
- 03:37:35 Dione
- 04:19:07 Marc Acardipane
- 05:33:20 Rob Gee
- 06:56:00 Promo
- 08:04:22 Panic & The Viper
- 09:50:18 AniMe
- 10:57:00 Andy The Core presents Hardcore Is Forever

**INDIGO**

- 00:23:30 Rude Convict
- 01:23:14 Amentis
- 02:24:00 Cold Confusion
- 03:42:06 Sanctuary
- 04:21:48 The Saints
- 05:22:00 Fraw
- 06:23:20 Physika
- 07:25:07 Zyon
- 08:23:45 Element
- 09:23:48 Radianze & Sub Sonik
- 10:23:18 Scarra
- 11:30:40 The Smiler

**MAGENTA**

- 00:23:50 Sunny D
- 01:54:00 Pavo
- 03:43:10 Zany
- 04:24:40 Davide Sonar
- 05:22:30 DJ Mystery
- 06:23:10 Tatanka
- 07:23:30 JDX
- 08:24:20 Luna
- 09:09:30 TNT
- 10:21:45 Donkey Rollers
- 10:35:00 Alpha Twins

**PURPLE**

- 01:20:20 Serzo
- 01:32:22 Crest
- 02:22:10 Redhot
- 02:51:06 Miss Isa
- 03:35:10 Dvastate
- 04:20:27 Spectre
- 05:06:50 Guardelion
- 05:52:40 Strike Blood
- 06:37:14 Erabreak
- 07:23:05 Storah
- 08:22:00 Kelvin Farheaven
- 08:51:32 Hyperverb
- 09:37:14 Demencia
- 10:21:10 Myth
- 11:06:24 Crazykill

**RED**

- Jones
- Warrior Workout
- Adrenalize & Galactixx
- Wildstylez
- Devin Wild
- POWER HOUR
- Daybreak Session with Geck-o
- Rooler
- Outsiders
- D-Block & S-te-Fan
- Warface
- Angerfist
- The Endshow

**SILVER**

- 01:28:41 Starving Insect
- 02:13:20 AVLM
- 03:28:40 Armageddon Project
- 04:13:50 Mindustries b2b Fracture 4
- 05:15:45 The Silence Live
- 06:14:00 Waldhaus
- 07:12:40 Catscan
- 08:13:50 The Outside Agency
- 09:43:00 Hellfish & Thrasher

**UV**

- 00:26:00 Refuzion
- 01:37:13 Psyko Punkz
- 02:29:24 Toneshifterz
- 03:46:00 Demi Kanon
- 04:27:40 Cyber
- 05:25:25 Maxtreme
- 06:42:33 Audiotricz & Ecstatic present Progressive Hardstyle
- 07:27:28 Primeshock
- 08:27:42 Bass Modulators
- 09:28:02 Frontliner
- 10:27:15 B-Front 20 Years
- 11:13:37 Darren Styles

**YELLOW**

- 00:19:50 Aradia
- 00:01:43 Remzcore
- 02:01:32 TukkerTempo
- 03:03:10 Juju Rush
- 04:04:25 Preceptor LIVE
- 04:34:25 Jur Terreur
- 05:19:03 Roza
- 06:17:22 The Chemicals of Noxiouz
- 06:53:34 Levenkhan
- 07:34:50 Doris presents Elixir
- 08:06:11 Juliex
- 08:52:15 Abaddon & MC Raise present Fallen Angel LIVE
- 09:17:24 STV
- 10:16:55 Unicorn on K presents PIPIPIPI LIVE
- 10:42:26 Akira


## Video content

All video content of Day 3 can be downloaded from [1gabba.pw](https://1gabba.pw/node/78646):

- Defqon.1.2024.Day.3.Red.1080p.WEB.x264

- Adrenalize-VS-Galactixx.RED.Saturday.Defqon1.2024.2160p.mp4
- Angerfist.RED.Saturday.Defqon1.2024.2160p.mp4
- D-Block-and-S-te-Fan.RED.Saturday.Defqon1.2024.2160p.mp4
- Devin-Wild.RED.Saturday.Defqon1.2024.2160p.mp4
- Endshow.RED.Saturday.Defqon1.2024.2160p.mp4
- Geck-o.RED.Saturday.Defqon1.2024.2160p.mp4
- Outsiders.RED.Saturday.Defqon1.2024.2160p.mp4
- Power-Hour.RED.Saturday.Defqon1.2024.2160p.mp4
- Rooler.RED.Saturday.Defqon1.2024.2160p.mp4
- Warface.RED.Saturday.Defqon1.2024.2160p.mp4
- Warrior-Workout.RED.Saturday.Defqon1.2024.2160p.mp4
- Wildstylez.RED.Saturday.Defqon1.2024.2160p.mp4

## Audio content

All audio content of Day 3 can be downloaded from [1gabba.pw](https://1gabba.pw/node/78646):

- VA_-_Defqon.1_2024_Day_3_Black-WEB-2024
- VA_-_Defqon.1_2024_Day_3_Blue-WEB-2024
- VA_-_Defqon.1_2024_Day_3_Gold-WEB-2024
- VA_-_Defqon.1_2024_Day_3_Indigo-WEB-2024
- VA_-_Defqon.1_2024_Day_3_Magenta-WEB-2024
- VA_-_Defqon.1_2024_Day_3_Purple-WEB-2024
- VA_-_Defqon.1_2024_Day_3_Red-WEB-2024
- VA_-_Defqon.1_2024_Day_3_Silver-WEB-2024
- VA_-_Defqon.1_2024_Day_3_UV-WEB-2024
- VA_-_Defqon.1_2024_Day_3_Yellow-WEB-2024


## DAY 4 - Sunday, June 30

**RED**



## Video content

All video content of Day 4 can be downloaded from [1gabba.pw](https://1gabba.pw/node/67916/defqon-1-2023-day-4-video-audio):



## Audio content

All audio content of Day 4 can be downloaded from [1gabba.pw](https://1gabba.pw/node/67916/defqon-1-2023-day-4-video-audio):



***
